package com.example.toshibin.coshproject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class ResultadoActivity extends AppCompatActivity {

    private RecyclerView recView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layMan;
    private ArrayList<Trabajador> datos = new ArrayList<>();
    private int tipoBusqueda;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);

        tipoBusqueda = getIntent().getIntExtra("tipoBusqueda", 1);
        obtenerListado();
    }

    private void llamar(int position) {
        Trabajador t = (Trabajador)datos.get(position);
        try{
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: "+t.getTelefono().toString()));
            startActivity(intent);
        }
        catch(SecurityException se){
        }
    }

    private void obtenerListado() {
        new ObtenerDatos(this).execute();
    }

    private class ObtenerDatos extends AsyncTask<Void,Void,String>{

        private final Context context;
        private ProgressDialog pg;

        public ObtenerDatos(Context context){
            this.context=context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pg= new ProgressDialog(context);
            pg.setIndeterminate(true);
            pg.setMessage("Buscando...");
            pg.setCancelable(false);
            pg.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String json="";
            try {
                json=WSDatos.getInstance().listaTrabajadores(tipoBusqueda);
            }
            catch(Exception e){

            }
            return json;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pg.hide();
            try{
                Gson gson= new Gson();
                datos = gson.fromJson(s, new TypeToken<List<Trabajador>>(){}.getType());
            }
            catch (Exception e){
            }
            pasar();
        }
    }

    private void pasar() {
        recView = (RecyclerView) findViewById(R.id.listado);
        recView.setHasFixedSize(true);
        layMan = new LinearLayoutManager(this);
        recView.setLayoutManager(layMan);
        recView.addItemDecoration(new DividerItem(this));

        if(tipoBusqueda == 1) {
            adapter = new EmergenciaAdapter(this, datos, tipoBusqueda, new EmergenciaAdapter.AdapterListener(){
                @Override
                public void btnLlamarOnClick(View v, int position) {
                    llamar(position);
                }
            });

        }
        else{
            adapter = new TrabajadorAdapter(this, datos);
            recView.addOnItemTouchListener(new RecViewClickListener(ResultadoActivity.this, new RecViewClickListener.OnItemClickListener() {

                @Override
                public void onItemClick(View view, int position) {
                    Intent i = new Intent(ResultadoActivity.this, ItemPerfilActivity.class);
                    i.putExtra("master", datos.get(position).toString());
                    i.putExtra("tipoBusqueda", tipoBusqueda);
                    i.putExtra("rubro", getIntent().getStringExtra("rubro"));
                    startActivity(i);
                }
            }));
        }
        recView.setAdapter(adapter);
    }
}
