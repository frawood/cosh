package com.example.toshibin.coshproject;


import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

/**
 * Created by toshibin on 01-06-2016.
 */
public class Sesion {

    private static Sesion instancia;
    private static final String nombre_instancia_shared="sesion";
    private static final String nombre_usuario_instancia="nombre_usuario_instancia";
    private static final String key_usuario_json="key_usuario_json";
    private static final String key_bd_json="key_bd_json";

    private Sesion(){}

    public static Sesion getInstancia(){
        if(instancia == null){
            instancia = new Sesion();
        }
        return instancia;
    }

    public void guardarSesion(Context context, String nombre){
        SharedPreferences sp=context.getSharedPreferences(nombre_instancia_shared, context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sp.edit();
        editor.putString(nombre_usuario_instancia,nombre);
        editor.commit();
    }

    public void guardarUsuario(Context context, String jsonUsuario){
        SharedPreferences sp=context.getSharedPreferences(nombre_instancia_shared, context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sp.edit();
        editor.putString(key_usuario_json,jsonUsuario);
        editor.commit();
    }
    public String obtenerUsuario(Context context) {
        SharedPreferences sp = context.getSharedPreferences(nombre_instancia_shared, Context.MODE_PRIVATE);
        return sp.getString(key_usuario_json, "");
    }

    /*public void guardarBD(Context context, String jsonBD){
        SharedPreferences sp= context.getSharedPreferences(nombre_instancia_shared, context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sp.edit();
        editor.putString(key_bd_json, jsonBD);
        editor.commit();
    }
    public String obtenerBD(Context context){
        SharedPreferences sp = context.getSharedPreferences(nombre_instancia_shared, Context.MODE_PRIVATE);
        return sp.getString(key_bd_json, "");
    }*/
}

