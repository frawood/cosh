package com.example.toshibin.coshproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;

public class IngresoTrabajadorActivity extends AppCompatActivity {

    private EditText txtDescripcion;
    private EditText txtEdad;
    private Button btnGuardar;
    private Spinner rubro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingreso_trabajador);

        btnGuardar = (Button)findViewById(R.id.BtnGuardarMaster);
        txtDescripcion = (EditText)findViewById(R.id.txtDescripcion);
        txtEdad = (EditText)findViewById(R.id.txtEdad);
        rubro = (Spinner)findViewById(R.id.tipoRubro);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(valida()){
                    Sesion.getInstancia().guardarSesion(IngresoTrabajadorActivity.this,getIntent().getStringExtra("nombre"));
                    Persona p=new Persona();
                    p.setNombre(getIntent().getStringExtra("nombre").toString());
                    p.setComuna(getIntent().getStringExtra("comuna").toString());
                    p.setDireccion(getIntent().getStringExtra("direccion").toString());
                    p.setTelefono(getIntent().getStringExtra("telefono").toString());

                    Gson gson=new Gson();
                    String jsonPersona = gson.toJson(p);
                    Sesion.getInstancia().guardarUsuario(IngresoTrabajadorActivity.this, jsonPersona);
                    Intent i=new Intent(IngresoTrabajadorActivity.this,BusquedaActivity.class);
                    startActivity(i);
                    finish();
                }
                else Toast.makeText(IngresoTrabajadorActivity.this, "Completar Campos", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean valida() {
        boolean validado;
        if(txtEdad.length()!=0&&txtDescripcion.length()!=0&&rubro.getSelectedItemPosition()!=0){
            validado=true;
        }
        else
            validado=false;
        return validado;
    }
}
