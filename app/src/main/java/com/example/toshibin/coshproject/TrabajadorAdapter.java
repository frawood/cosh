package com.example.toshibin.coshproject;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by toshibin on 08-07-2016.
 */
public class TrabajadorAdapter extends RecyclerView.Adapter<TrabajadorAdapter.ViewHolder>{

    private final Context context;
    private final ArrayList<Trabajador> datos;

    public TrabajadorAdapter(Context context, ArrayList<Trabajador> inDatos){
        this.context = context;
        this.datos = inDatos;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.trabajador_item, viewGroup, false);
        ViewHolder vh=new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int i) {
        final Trabajador t=datos.get(i);
        holder.txtNombre.setText(t.getNombre().toString());
        holder.txtUbicacion.setText(t.getComuna().toString());
        holder.btnLlamar.setImageResource(R.drawable.flecha);
        holder.rtbar.setRating((int)t.getCalificacion());
        try{
            Picasso.with(context).load(t.getUrlFoto()).transform(new FotoCirc()).into(holder.foto);
        }
        catch (Exception e){e.printStackTrace();}
    }

    @Override
    public int getItemCount() {
        try{
            return datos.size();
        }
        catch (NullPointerException e){

        }
        return 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtNombre;
        public TextView txtUbicacion;
        public RatingBar rtbar;
        public ImageButton btnLlamar;
        public ImageView foto;

        public ViewHolder(View v) {
            super(v);
            txtNombre = (TextView)v.findViewById(R.id.txtNombre);
            txtUbicacion = (TextView)v.findViewById(R.id.txtUbicacion);
            rtbar = (RatingBar) v.findViewById(R.id.ratingBar);
            btnLlamar = (ImageButton)v.findViewById(R.id.btnLlamar);
            foto = (ImageView)v.findViewById(R.id.preFoto);
        }
    }
}


