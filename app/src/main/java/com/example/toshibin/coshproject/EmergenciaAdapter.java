package com.example.toshibin.coshproject;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;


import java.util.ArrayList;


/**
 * Created by toshibin on 01-06-2016.
 */
public class EmergenciaAdapter extends RecyclerView.Adapter<EmergenciaAdapter.ViewHolder>{

    private final Context context;
    private final ArrayList<Trabajador> datos;
    private final int tipoBusqueda;
    private int donde;

    public EmergenciaAdapter(Context context, ArrayList<Trabajador> inDatos, int tipoBusqueda, AdapterListener listener){
        this.context = context;
        this.datos = inDatos;
        this.tipoBusqueda=tipoBusqueda;
        onCLickListener = listener;
    }

    public AdapterListener onCLickListener;
    public interface AdapterListener{
        void btnLlamarOnClick(View v, int position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.emergencia_item, viewGroup, false);
        ViewHolder vh=new ViewHolder(v);
        vh.btnLlamar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCLickListener.btnLlamarOnClick(v, donde);
            }
        });
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int i) {
        final Trabajador t=datos.get(i);
        donde=i;
        holder.txtNombre.setText(t.getNombre().toString());
        holder.txtUbicacion.setText("Ubicación: a " + t.getUbicacion() + " metros");
        holder.rtbar.setRating((int)t.getCalificacion());
    }

    @Override
    public int getItemCount() {
        try{
            return datos.size();
        }
        catch (NullPointerException e){

        }
        return 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtNombre;
        public TextView txtUbicacion;
        public RatingBar rtbar;
        public ImageButton btnLlamar;

        public ViewHolder(View v) {
            super(v);
            txtNombre = (TextView)v.findViewById(R.id.txtNombre);
            txtUbicacion = (TextView)v.findViewById(R.id.txtUbicacion);
            rtbar = (RatingBar) v.findViewById(R.id.ratingBar);
            btnLlamar = (ImageButton)v.findViewById(R.id.btnLlamar);
        }
    }
}
