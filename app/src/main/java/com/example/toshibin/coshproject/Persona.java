package com.example.toshibin.coshproject;


/**
 * Created by toshibin on 31-05-2016.
 */
public class Persona {
    private String nombre;
    private String direccion;
    private String comuna;
    private String telefono;

    public Persona() {
    }

    public Persona(String nombre, String direccion, String comuna, String telefono) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.comuna = comuna;
        this.telefono = telefono;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setComuna(String comuna) {
        this.comuna = comuna;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getComuna() {
        return comuna;
    }

    public String getTelefono() {
        return telefono;
    }

    @Override
    public String toString() {
        return "{nombre: '" + nombre +
                "', direccion: '" + direccion +
                "', comuna: '" + comuna +
                "', telefono: '" + telefono+
                "',";
    }
}

