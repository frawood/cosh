package com.example.toshibin.coshproject;


public class Trabajador extends Persona {

    private int rubro;
    private String descripcion;
    private int ubicacion;
    private int calificacion = 0;
    private int edad;
    private String urlFoto;

    public Trabajador() {
    }

    public Trabajador(String nombre, String direccion, String comuna, String telefono, int edad, int rubro,
                      String descripcion, int ubicacion, String urlFoto) {
        super(nombre, direccion, comuna, telefono);
        this.setRubro(rubro);
        this.setDescripcion(descripcion);
        this.setUbicacion(ubicacion);
        this.setEdad(edad);
        this.setUrlFoto(urlFoto);
    }

    public int getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(int calificacion) {
        this.calificacion = calificacion;
    }

    public int getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(int ubicacion) {
        this.ubicacion = ubicacion;
    }

    public void setRubro(int rubro) {
        this.rubro = rubro;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getRubro() {
        return rubro;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    @Override
    public String toString() {
        return super.toString()+"edad: '"+edad+
                "', rubro: '" + rubro +
                "', descripcion: '" + descripcion +
                "', ubicacion: '" + ubicacion +
                "', calificacion: '" + calificacion+
                "', urlFoto: '" + urlFoto+
                "'}";
    }
}

