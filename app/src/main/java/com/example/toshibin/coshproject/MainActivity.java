package com.example.toshibin.coshproject;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Button btnCliente;
    private Button btnTrabajador;
    private EditText txtNombre;
    private EditText txtDireccion;
    private EditText txtComuna;
    private EditText txtTelefono;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnCliente=(Button)findViewById(R.id.btnCliente);
        btnTrabajador = (Button)findViewById(R.id.btnTrabajador);
        txtNombre=(EditText)findViewById(R.id.txtNombre);
        txtDireccion =(EditText)findViewById(R.id.txtDireccion);
        txtComuna=(EditText)findViewById(R.id.txtComuna);
        txtTelefono=(EditText)findViewById(R.id.txtTelefono);

        btnCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ingresarCliente();
            }
        });
        btnTrabajador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ingresarTrabajador();
            }
        });
    }

    private void ingresarTrabajador() {
        if(valida()){
            Intent i = new Intent(this, IngresoTrabajadorActivity.class);
            i.putExtra("nombre",txtNombre.getText().toString());
            i.putExtra("comuna", txtComuna.getText().toString());
            i.putExtra("direccion", txtDireccion.getText().toString());
            i.putExtra("telefono", txtTelefono.getText().toString());
            startActivity(i);
            finish();
        }
        else Toast.makeText(this, "Completar Campos", Toast.LENGTH_SHORT).show();
    }

    private void ingresarCliente() {
        if(valida()){
            Sesion.getInstancia().guardarSesion(this, txtNombre.getText().toString());
            guardarUsuario();
            cambiarPreferencia();

        }
        else Toast.makeText(this, "Completar Campos", Toast.LENGTH_SHORT).show();
    }

    private void guardarUsuario() {
        Persona p=new Persona();
        p.setNombre(txtNombre.getText().toString());
        p.setComuna(txtComuna.getText().toString());
        p.setDireccion(txtDireccion.getText().toString());
        p.setTelefono(txtTelefono.getText().toString());

        Gson gson=new Gson();
        String jsonPersona = gson.toJson(p);
        Sesion.getInstancia().guardarUsuario(this, jsonPersona);
    }


    private void cambiarPreferencia() {
        Intent i=new Intent(this,BusquedaActivity.class);
        startActivity(i);
        finish();
    }

    private Boolean valida(){
        Boolean validacion;
        if(txtNombre.length()!=0&& txtDireccion.length()!=0&&txtComuna.length()!=0&&txtTelefono.length()!=0){
            validacion=true;
        }
        else
        {
            validacion=false;
        }
        return validacion;
    }
}

