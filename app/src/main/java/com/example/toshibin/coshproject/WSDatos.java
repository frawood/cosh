package com.example.toshibin.coshproject;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by toshibin on 25-06-2016.
 */
public class WSDatos {

    private static final long TIME_OUT_SECONDS = 60;
    private static WSDatos instance;

    public static WSDatos getInstance() {
        if (instance == null) {
            instance = new WSDatos();
        }
        return instance;
    }

    private WSDatos(){}

    public String listaTrabajadores(int tipoB){
        String respuesta= "";
        String url = "";
        if(tipoB == 1)
            url = "http://www.json-generator.com/api/json/get/bTQQDabooi?indent=2";
        else
            url = "http://www.json-generator.com/api/json/get/cpCohmsnaW?indent=2";
        try {
            respuesta = getRequest(url);
        }
        catch (Exception e) {
        }
        return respuesta;
    }

    /*private String postRequest(String url, FormEncodingBuilder params) throws IOException {
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(TIME_OUT_SECONDS, TimeUnit.SECONDS); // connect timeout
        client.setReadTimeout(TIME_OUT_SECONDS, TimeUnit.SECONDS);

        RequestBody formBody = params.build();

        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();

        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
        String respuesta = response.body().string();
        return respuesta;
    }*/

    private String getRequest(String url)  throws IOException {
        OkHttpClient cliente = new OkHttpClient.Builder().connectTimeout(TIME_OUT_SECONDS,TimeUnit.SECONDS)
                .readTimeout(TIME_OUT_SECONDS,TimeUnit.SECONDS) .build();
        Request request = new Request.Builder()
                .url(url)
                .get()
                .addHeader("cache-control", "no-cache")
                .build();

        Response response = cliente.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
        String respuesta = response.body().string();
        return respuesta;
    }
}

