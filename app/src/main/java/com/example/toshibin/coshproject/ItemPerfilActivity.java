package com.example.toshibin.coshproject;


import android.content.Intent;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

public class ItemPerfilActivity extends AppCompatActivity {

    private TextView txtNombre;
    private TextView txtEdad;
    private TextView txtDireccion;
    private TextView txtComuna;
    private TextView txtTelefono;
    private TextView txtRubro;
    private TextView txtDescripcion;
    private RatingBar calificacion;
    private ImageView foto;
    private ProgressBar pb;
    private Button btnAgregar;
    private ImageButton btnTel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_perfil);

        Gson gson = new Gson();
        final Trabajador t = gson.fromJson(getIntent().getStringExtra("master"), Trabajador.class);

        txtNombre = (TextView) findViewById(R.id.masterNombre);
        txtEdad = (TextView)findViewById(R.id.masterEdad);
        txtComuna = (TextView) findViewById(R.id.masterComuna);
        txtDireccion=(TextView)findViewById(R.id.masterDirec);
        txtTelefono=(TextView)findViewById(R.id.masterTelefono);
        txtRubro = (TextView)findViewById(R.id.masterRubro);
        txtDescripcion = (TextView)findViewById(R.id.masterDescripcion);
        calificacion = (RatingBar) findViewById(R.id.masterCalificacion);
        foto = (ImageView)findViewById(R.id.foto);
        btnAgregar = (Button)findViewById(R.id.btnAgregar);
        btnTel =(ImageButton)findViewById(R.id.btnLlamarMaster);
        pb = (ProgressBar)findViewById(R.id.pBar);

        try{
            Picasso.with(this).load(t.getUrlFoto()).into(this.foto);
            pb.setVisibility(View.GONE);
        }
        catch (Exception e){e.printStackTrace();}
        txtNombre.setText(t.getNombre().toString());
        txtEdad.setText("Edad: "+t.getEdad()+" años");
        txtComuna.setText("Comuna: "+t.getComuna().toString());
        txtDireccion.setText("Dirección: "+t.getDireccion().toString());
        txtTelefono.setText("Teléfono de contacto: "+t.getTelefono().toString());
        txtRubro.setText("Especialidad: "+getIntent().getStringExtra("rubro").toString());
        txtDescripcion.setText("Descripción Personal: "+t.getDescripcion().toString());
        calificacion.setRating((int)t.getCalificacion());

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_INSERT);
                i.setType(ContactsContract.RawContacts.CONTENT_TYPE);
                i.putExtra(ContactsContract.Intents.Insert.NAME,t.getNombre().toString());
                i.putExtra(ContactsContract.Intents.Insert.PHONE,t.getTelefono().toString());
                i.putExtra(ContactsContract.Intents.Insert.PHONE_TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE);
                startActivity(i);
            }
        });
        btnTel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: "+t.getTelefono().toString()));
                    startActivity(intent);
                }
                catch(SecurityException se){
                }
            }
        });

    }
}
